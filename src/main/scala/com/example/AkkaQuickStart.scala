//#full-example
package com.example

import akka.actor.typed.ActorSystem
import com.example.GreeterMain.SayHello

//#main-class
object AkkaQuickStart extends App {
  //#actor-system
  val greeterMain: ActorSystem[GreeterMain.SayHello] = ActorSystem(GreeterMain(), "AkkaQuickStart")
  //#actor-system

  //#main-send-messages
  greeterMain ! SayHello("Charles")
  //#main-send-messages
}
//#full-example
