package com.sma

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import com.sma.GridVisualisation.CellReport

object GridCell {
  sealed trait Message
  final case class PopulateCell(s: Char) extends Message
  final case class FillCell(s: Char, replyTo: ActorRef[Message]) extends Message
  final case class No() extends Message
  final case class SetNeighbours(
    north: Option[ActorRef[Message]],
    south: Option[ActorRef[Message]],
    east: Option[ActorRef[Message]],
    west: Option[ActorRef[Message]],
  ) extends Message

  case class State(
    i: Int,
    j: Int,
    population: Option[Char],
    gridVisualisation: ActorRef[CellReport],
    north: Option[ActorRef[Message]],
    south: Option[ActorRef[Message]],
    east: Option[ActorRef[Message]],
    west: Option[ActorRef[Message]],
  )

  def apply(i: Int, j: Int, gridVisualisation: ActorRef[CellReport]): Behavior[Message] = {
    state(State(i, j, None, gridVisualisation, None, None, None, None))
  }

  private def sendMessageToNextCell(context: ActorContext[Message], count: Int, s: State): Unit = {
    val nextCount = if(count < 9) count + 1 else 0
    val definedNeighbours = Array(s.north, s.south, s.east, s.west).filter(_.isDefined)
    val randomNeighbour = definedNeighbours((math.random() * definedNeighbours.length).toInt).get
    randomNeighbour ! FillCell(nextCount.toString.head, context.self)
  }

  private def state(s: State): Behavior[Message] = Behaviors.receive { (context, message) =>
    message match {
      case No() =>
        sendMessageToNextCell(context, s.population.get.toString.toInt, s)
        state(s)
      case PopulateCell(p) =>
        val count = p.toString.toInt
        s.gridVisualisation ! CellReport(s.i, s.j, p)
        sendMessageToNextCell(context, count, s)
        state(s.copy(population = Some(p)))
      case FillCell(p, replyTo) =>
        if(s.population.isDefined) {
          replyTo ! No()
          state(s)
        }
        else {
          val count = p.toString.toInt
          s.gridVisualisation ! CellReport(s.i, s.j, p)
          sendMessageToNextCell(context, count, s)
          state(s.copy(population = Some(p)))
        }
      case SetNeighbours(north, south, east, west) =>
        state(s.copy(north = north, south = south, east = east, west = west))
    }
  }

}
