package com.sma

import akka.actor.typed.ActorSystem
import com.sma.GridMain.Populate

object AkkaStart extends App {

  val width = 5
  val height = 5

  val gridMain: ActorSystem[GridMain.Message] = ActorSystem(GridMain(width, height), "SMA-SIM")
  gridMain ! Populate(2, 2, '0')
}
