package com.sma

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors

object GridVisualisation {
  final case class CellReport(i: Int, j: Int, s: Char)

  def apply(width: Int, height: Int): Behavior[CellReport] = {
    state(width, height, Array.fill(width, height)(' '))
  }

  private def state(
    width: Int,
    height: Int,
    data: Array[Array[Char]]
  ): Behavior[CellReport] = Behaviors.receive((context, message) => {
    data(message.i)(message.j) = message.s

    context.log.info("\n" + visualiseGrid(width, data))
    state(width, height, data)
  })

  private def visualiseGrid(width: Int, data: Array[Array[Char]]): String = {
    val firstRow: String = "~" + "~" * width * 2 + "~"
    val dataRows: Array[String] = data.map(row => "|" + row.map(_.toString + " ").reduce(_ + _) + "|")
    val lastRow: String = "~" + "~" * width * 2 + "~"

    (firstRow +: dataRows :+ lastRow).reduce(_ + "\n" + _)
  }
}
