package com.sma

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import com.sma.GridCell.{PopulateCell, SetNeighbours}

import scala.util.Try

object GridMain {
  sealed trait Message
  final case class Populate(i: Int, j: Int, p: Char) extends Message

  def apply(width: Int, height: Int): Behavior[Message] =
    Behaviors.setup { context =>

      val gridVisualisation = context.spawn(GridVisualisation(width, height), "viz")

      val gridCells =
        (0 until height).toArray.map { i =>
          (0 until width).toArray.map { j =>
            (i, j, context.spawn(GridCell(i, j, gridVisualisation), s"cell:$i,$j"))
          }
        }
      val flatGridCells = gridCells.flatten

      flatGridCells.foreach { case (i, j, gridCell) =>
        val northCell = Try(gridCells(i - 1)(j)._3).toOption
        val southCell = Try(gridCells(i + 1)(j)._3).toOption
        val eastCell = Try(gridCells(i)(j - 1)._3).toOption
        val westCell = Try(gridCells(i)(j + 1)._3).toOption

        gridCell ! SetNeighbours(northCell, southCell, eastCell, westCell)
      }

      Behaviors.receiveMessage {
        case Populate(gridI, gridJ, p) =>
          flatGridCells
            .filter { case (i, j, _) => i == gridI && j == gridJ}
            .head._3 ! PopulateCell(p)
          Behaviors.same
      }
    }
}
